import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  formulario!: FormGroup;
  contenido2: string[] = []

  constructor(private fb: FormBuilder) { 
    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  crearFormulario(): void {
    this.formulario = this.fb.group({
      cajaG: [''],
      cajaP: this.fb.array([[]])
    })
  }

  get contenidoP(){
    return this.formulario.get('cajaP') as FormArray
  }

  agregar(): void{
    this.contenidoP.push(this.fb.control(''))
  }

  borrarCaja(i: number): void{
    this.contenidoP.removeAt(i);
  }

  limpiarCajaP(i: number){
    console.log(this.formulario.value.cajaP[i]);
    this.formulario.value.cajaP[i]
  }

  limpiar():void{
    this.contenido2 = ['']
    this.formulario.reset
  }

  limpiarCajaG(): void{
    console.log('guardar');
    this.contenido2 = this.formulario.value.cajaP
  }

}
